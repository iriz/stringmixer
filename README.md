Building the application
========================
`./mvnw clean package`



Running the application
========================
`./mvnw spring-boot:run`


API
===

**Calling the API**

```

curl -XPUT 'http://localhost:8080/mix' -H 'Content-Type: application/json' -d'
{
	"string1": "my&friend&Paul has heavy hats! &",
	"string2": "my friend John has many many friends &"
}
'
```