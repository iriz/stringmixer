package net.p.i.stringmixer;

import org.hibernate.validator.constraints.NotBlank;

public class MixingRequest {

    @NotBlank(message="string1 is required")
    private String string1;
    @NotBlank(message="string2 is required")
    private String string2;

    public String getString1() {
        return string1;
    }

    public void setString1(String string1) {
        this.string1 = string1;
    }

    public String getString2() {
        return string2;
    }

    public void setString2(String string2) {
        this.string2 = string2;
    }
}
