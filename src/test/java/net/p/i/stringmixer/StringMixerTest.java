package net.p.i.stringmixer;

import org.junit.Test;

import static net.p.i.stringmixer.StringMixer.mix;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class StringMixerTest {


    @Test
    public void returns_empty_string_given_two_strings_are_same(){
        final String string1 = "my&friend&Paul has heavy hats! &";
        final String string2 = "my&friend&Paul has heavy hats! &";
        assertThat(mix(string1,string2), is(""));
    }

    @Test
    public void should_mix_strings() {

        final String string1 = "my&friend&Paul has heavy hats! &";
        final String string2 = "my friend John has many many friends &";
        assertThat(mix(string1, string2), is("2:nnnnn/1:aaaa/1:hhh/2:mmm/2:yyy/2:dd/2:ff/2:ii/2:rr/=:ee/=:ss"));

        final String string3 = "mmmmm m nnnnn y&friend&Paul has heavy hats! &";
        final String string4 = "my frie n d Joh n has ma n y ma n y frie n ds n&";
        assertThat(mix(string3, string4), is("1:mmmmmm/=:nnnnnn/1:aaaa/1:hhh/2:yyy/2:dd/2:ff/2:ii/2:rr/=:ee/=:ss"));

        final String string5 = "Are the kids at home? aaaaa fffff";
        final String string6 = "Yes they are here! aaaaa fffff";
        assertThat(mix(string5, string6), is("=:aaaaaa/2:eeeee/=:fffff/1:tt/2:rr/=:hh"));
    }

}