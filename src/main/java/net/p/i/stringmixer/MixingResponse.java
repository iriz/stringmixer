package net.p.i.stringmixer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MixingResponse {

    private final String string1;
    private final String string2;
    private final String mixedString;

    public MixingResponse(@JsonProperty("string1") String string1,
                          @JsonProperty("string2") String string2,
                          @JsonProperty("mixedString") String mixedString) {
        this.string1 = string1;
        this.string2 = string2;
        this.mixedString = mixedString;
    }

    public String getString1() {
        return string1;
    }

    public String getString2() {
        return string2;
    }

    public String getMixedString() {
        return mixedString;
    }
}
