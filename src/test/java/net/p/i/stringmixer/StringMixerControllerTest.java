package net.p.i.stringmixer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= RANDOM_PORT)
public class StringMixerControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void should_return_bad_request_given_validation_error(){
        //when
        ResponseEntity<List> responseEntity = restTemplate.exchange("/mix", PUT, new HttpEntity<>(new MixingRequest()), List.class);
        //then
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode(), is(BAD_REQUEST));
        assertThat((List<String>)responseEntity.getBody(), containsInAnyOrder(
                "string1 is required",
                "string2 is required"));
    }

    @Test
    public void should_return_mixed_string(){
        //when
        final MixingRequest request = newMixingRequest("my&friend&Paul has heavy hats! &", "my friend John has many many friends &");
        ResponseEntity<MixingResponse> responseEntity = restTemplate.exchange("/mix", PUT, new HttpEntity<>(request), MixingResponse.class);
        //then
        assertNotNull(responseEntity);
        assertThat(responseEntity.getStatusCode(), is(OK));
        assertThat(responseEntity.getBody().getString1(), is(request.getString1()));
        assertThat(responseEntity.getBody().getString2(), is(request.getString2()));
        assertThat(responseEntity.getBody().getMixedString(), is("2:nnnnn/1:aaaa/1:hhh/2:mmm/2:yyy/2:dd/2:ff/2:ii/2:rr/=:ee/=:ss"));
    }

    private static MixingRequest newMixingRequest(String s1, String s2){
         MixingRequest mixingRequest = new MixingRequest();
        mixingRequest.setString1(s1);
        mixingRequest.setString2(s2);
        return mixingRequest;
    }

}