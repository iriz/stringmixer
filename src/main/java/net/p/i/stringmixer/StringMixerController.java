package net.p.i.stringmixer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static net.p.i.stringmixer.StringMixer.mix;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@RestController
public class StringMixerController {


    @PutMapping("/mix")
    public ResponseEntity mixStrings(@Valid @RequestBody MixingRequest request,
                                     BindingResult errors){
        if(errors.hasErrors()){
            return badRequest().body(toErrorMessages(errors));
        }

        final String mixedString = mix(request.getString1(), request.getString2());
        return ok().body(new MixingResponse(request.getString1(), request.getString2(), mixedString));
    }



    private List<String> toErrorMessages(BindingResult errors) {
        return errors.getAllErrors()
                .stream().map(e -> e.getDefaultMessage())
                .collect(toList());
    }

}
