package net.p.i.stringmixer;

import java.util.AbstractMap.SimpleEntry;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

public class StringMixer {

    private static final String ALL_LOWER_CASE_CHARS = "abcdefghijklmnopqrstuvwxyz";
    private static final Comparator<String> STRING_COMPARATOR = (o1, o2) -> Integer.compare(o2.length(), o1.length()) == 0 ? o1.compareTo(o2) : Integer.compare(o2.length(), o1.length());

    public static String mix(String string1, String string2) {
        if(string1.equals(string2)) return "";
        final List<CharacterCount> characterCounts = characterCountsInString(string1, string2);
        return characterCounts.stream()
                .map(StringMixer::mixedStringValue)
                .sorted(STRING_COMPARATOR)
                .collect(Collectors.joining("/"));
    }

    private static String mixedStringValue(CharacterCount characterCount) {
        return prefix(characterCount).concat(stringValue(characterCount));
    }

    private static String stringValue(CharacterCount characterCount) {
       return IntStream.range(0, characterCount.getMaxCount())
                .mapToObj(i-> characterCount.character.toString())
                .collect(joining());
    }

    private static String prefix(CharacterCount characterCount){
        if (characterCount.countInString2 > characterCount.countInString1) {
            return "2:";
        } else if (Objects.equals(characterCount.countInString1, characterCount.countInString2)) {
            return "=:";
        }
        return "1:";
    }

    private static CharacterCount characterCounts(Character key, Integer countInString1, Integer countInString2) {
        return new CharacterCount(key, countInString1, countInString2);
    }

    private static List<CharacterCount> characterCountsInString(String string1, String string2) {
        Map<Character, Integer> string1CharCounts = lowerCaseCharacterCounts(string1);
        Map<Character, Integer> string2CharCounts = lowerCaseCharacterCounts(string2);

        return string1CharCounts.entrySet()
                .stream()
                .map(e -> characterCounts(e.getKey(), e.getValue(), string2CharCounts.get(e.getKey())))
                .filter(characterCount ->  characterCount.getMaxCount() > 1)
                .collect(toList());
    }

    private static Map<Character, Integer> lowerCaseCharacterCounts(String aValue) {
        final Map<Character, Long> charsCountInValue =  aValue.chars()
                .filter(Character::isLowerCase)
                .mapToObj(c -> (char) c)
                .collect(groupingBy(identity(), counting()));

        return ALL_LOWER_CASE_CHARS.chars()
                .mapToObj(c -> (char) c)
                .map(c -> charsCountInValue.containsKey(c)? new SimpleEntry<>(c, charsCountInValue.get(c).intValue()) : new SimpleEntry<>(c, 0))
                .collect(toMap(SimpleEntry::getKey, SimpleEntry::getValue));
    }

    private static class CharacterCount{
        public final Character character;
        public final Integer countInString1;
        public final Integer countInString2;

        public CharacterCount(Character character, Integer countInString1, Integer countInString2) {
            this.character = character;
            this.countInString1 = countInString1;
            this.countInString2 = countInString2;
        }

        public Integer getMaxCount(){
            return countInString1 > countInString2? countInString1 : countInString2;
        }
    }

}

